FROM node

WORKDIR /app

COPY /server_app/package.json /server_app/yarn.lock ./

RUN yarn

COPY ./server_app .
COPY .env .

EXPOSE 4000

CMD ["npm", "run", "serve"]