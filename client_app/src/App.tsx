import React from 'react';
import { CssBaseline } from '@material-ui/core';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { NavBar } from './components/NavBar';
import Top from './components/Top';
import Movies from './pages/Movies';
import CardForm from './pages/MovieForm';
import { Footer } from './components/Footer';
import { useStyles } from './styles/contentStyles';
import { GET_MOVIES } from './queries/MovieQuery';

import AuthRoute from './utils/AuthRoute';
import Login from './pages/Login';
import Register from './pages/Register';
import ContextProvider from './context';

export default function MainPage() {
  const { footer, container } = useStyles();

  const [CARD_QUERY, setCardsQuery] = React.useState(GET_MOVIES);
  const [queryVariable, setQueryVariable] = React.useState<undefined | {}>(
    undefined,
  );

  const CardsComponent = (props: any) => {
    return (
      <Movies props={props} QUERY={CARD_QUERY} variables={queryVariable} />
    );
  };

  return (
    <div className={container}>
      <ContextProvider>
        <CssBaseline />
        <NavBar
          setCardsQuery={setCardsQuery}
          setQueryVariable={setQueryVariable}
        />
        <main>
          <Router>
            <Top />
            <Route exact path="/" component={CardsComponent} />
            <Route exact path="/card" component={CardForm} />
            <AuthRoute exact path="/login" component={Login} />
            <AuthRoute exact path="/register" component={Register} />
          </Router>
        </main>
        <footer className={footer}>
          <Footer />
        </footer>
      </ContextProvider>
    </div>
  );
}
