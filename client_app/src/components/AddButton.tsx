import React from 'react';
import { Button } from '@material-ui/core';
import IconAdd from '@material-ui/icons/Add';
import { useStyles } from '../styles/contentStyles';

interface AddButtonProps {
  onClickAdd: () => void;
}

export default function AddButton({ onClickAdd }: AddButtonProps) {
  const classes = useStyles();

  return (
    <div className={classes.form}>
      <Button
        size="small"
        color="primary"
        style={{ width: '100px' }}
        onClick={onClickAdd}
      >
        <IconAdd />
      </Button>
    </div>
  );
}
