import React, { FC } from 'react';
import {
  Chip,
  FormControl,
  InputLabel,
  Select,
  Input,
  MenuItem,
  Theme,
  useTheme,
} from '@material-ui/core';

import { useStyles } from '../styles/contentStyles';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(value: string, values: string[], theme: Theme) {
  return {
    fontWeight:
      values.indexOf(value) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

interface Props {
  label: string;
  arrayOfChosen: string[];
  arrayValues: string[];
  onChange: (event: React.ChangeEvent<{ value: unknown }>) => void;
}

export const MultipleSelect: FC<Props> = ({
  arrayOfChosen,
  arrayValues,
  label,
  onChange,
}) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <FormControl>
      <InputLabel id="mutiple-chip-label">{label}</InputLabel>
      <Select
        labelId="mutiple-chip-label"
        id="mutiple-chip"
        multiple
        value={arrayOfChosen}
        onChange={onChange}
        input={<Input id="select-multiple-chip" />}
        renderValue={(selected) => (
          <div className={classes.chips}>
            {(selected as string[]).map((value, index) => (
              <Chip key={index} label={value} className={classes.chip} />
            ))}
          </div>
        )}
        MenuProps={MenuProps}
      >
        {arrayValues.map((value, index) => (
          <MenuItem
            key={index}
            value={value}
            style={getStyles(value, arrayOfChosen, theme)}
          >
            {value}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
