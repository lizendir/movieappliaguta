import React, { FC, useContext, Dispatch } from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  ButtonGroup,
  Button,
} from '@material-ui/core';
import { DocumentNode } from 'graphql';
import MovieIcon from '@material-ui/icons/Movie';

import { SearchButton } from './SearchButton';
import { GET_MOVIES } from '../queries/MovieQuery';
import { useStyles } from '../styles/contentStyles';
import { AuthContext } from '../context/auth';

interface BarAttr {
  setCardsQuery: Dispatch<React.SetStateAction<DocumentNode>>;
  setQueryVariable: Dispatch<React.SetStateAction<{} | undefined>>;
}

export const NavBar: FC<BarAttr> = ({ setCardsQuery, setQueryVariable }) => {
  const { icon, navbarTitle, logRegTabs } = useStyles();
  const context = useContext(AuthContext);

  const onClickMovieIcon = () => {
    setCardsQuery(GET_MOVIES);
    setQueryVariable(undefined);
  };

  const AuthBarItem = context?.user ? (
    <>
      <Typography className={logRegTabs} color="inherit">
        {context?.user ? context.user.username : 'Unknown'}
      </Typography>

      <ButtonGroup
        className={logRegTabs}
        color="inherit"
        aria-label="outlined primary button group"
      >
        <Button title="Logout" onClick={context.logout}>
          Logout
        </Button>
      </ButtonGroup>
    </>
  ) : (
    <ButtonGroup
      className={logRegTabs}
      color="inherit"
      aria-label="outlined primary button group"
    >
      <Button title="Login" href="/login">
        Login
      </Button>
      <Button title="Register" href="/register">
        Register
      </Button>
    </ButtonGroup>
  );

  return (
    <AppBar position="relative">
      <Toolbar>
        <Button className={icon} variant="contained" href="/">
          <MovieIcon onClick={onClickMovieIcon} to="/" />
        </Button>
        <Typography className={navbarTitle} variant="h5" color="inherit" noWrap>
          Movie Application
        </Typography>
        <SearchButton
          setCardsQuery={setCardsQuery}
          setQueryVariable={setQueryVariable}
        />
        {AuthBarItem}
      </Toolbar>
    </AppBar>
  );
};
