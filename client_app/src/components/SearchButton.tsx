import React, { Dispatch, FC, useState, useContext, useEffect } from 'react';
import { Button, Menu, MenuItem, Fade } from '@material-ui/core';
import { useQuery } from 'react-apollo';
import { DocumentNode } from 'graphql';

import { QueryNavBarData, Genres } from '../models/MovieModels';
import {
  NAVBAR_QUERY,
  GET_MOVIES_BY_ACTOR_QUERY,
  GET_MOVIES_BY_GENRE_QUERY,
} from '../queries/NavBarQuery';
import { useStyles } from '../styles/contentStyles';
import { ActorsContext } from '../context/actors';

interface BarAttr {
  setCardsQuery: Dispatch<React.SetStateAction<DocumentNode>>;
  setQueryVariable: Dispatch<React.SetStateAction<{} | undefined>>;
}

export const SearchButton: FC<BarAttr> = ({
  setCardsQuery,
  setQueryVariable,
}) => {
  const { btn } = useStyles();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [anchorElGenres, setAnchorElGenres] = useState<null | HTMLElement>(
    null,
  );
  const [anchorElActors, setAnchorElActors] = useState<null | HTMLElement>(
    null,
  );

  const { data, loading, error } = useQuery<QueryNavBarData>(NAVBAR_QUERY);

  const actorsContext = useContext(ActorsContext);

  useEffect(() => {
    if (!actorsContext.actors && data?.getActors) {
      actorsContext.setActors(data.getActors);
    }
  });

  const onClickFindByButton = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const onClickGenres = (event: React.MouseEvent<HTMLLIElement>) => {
    setAnchorElGenres(event.currentTarget);
  };

  const onClickActors = (event: React.MouseEvent<HTMLLIElement>) => {
    setAnchorElActors(event.currentTarget);
  };

  const chooseActor = (id: number) => {
    setCardsQuery(GET_MOVIES_BY_ACTOR_QUERY);
    setQueryVariable({ id: id });
    handleClose();
  };

  const chooseGenre = (genre: Genres) => {
    setCardsQuery(GET_MOVIES_BY_GENRE_QUERY);
    setQueryVariable({ genre: genre });
    handleClose();
  };

  const handleClose = () => {
    setAnchorEl(null);
    setAnchorElGenres(null);
    setAnchorElActors(null);
  };

  if (error || !data || loading)
    return (
      <Button className={btn} variant="outlined" disabled>
        Find by
      </Button>
    );

  return (
    <>
      <Button
        className={btn}
        variant="outlined"
        aria-controls="menu"
        aria-haspopup="true"
        onClick={onClickFindByButton}
        disabled={loading && error ? true : false}
      >
        Find by
      </Button>
      <Menu
        id="menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem
          aria-controls="list-genres"
          aria-haspopup="true"
          onClick={onClickGenres}
        >
          Genres
        </MenuItem>
        <MenuItem
          aria-controls="list-actors"
          aria-haspopup="true"
          onClick={onClickActors}
        >
          Actors
        </MenuItem>
      </Menu>
      <Menu
        id="list-genres"
        anchorEl={anchorElGenres}
        keepMounted
        open={Boolean(anchorElGenres)}
        onClose={handleClose}
      >
        {data.getGenres.map((value, index) => {
          return (
            <MenuItem key={index} onClick={(e) => chooseGenre(value.name)}>
              {value.name}
            </MenuItem>
          );
        })}
      </Menu>

      <Menu
        id="list-actors"
        anchorEl={anchorElActors}
        keepMounted
        open={Boolean(anchorElActors)}
        onClose={handleClose}
      >
        {data.getActors.map((value, index) => {
          return (
            <MenuItem key={index} onClick={(e) => chooseActor(value.id)}>
              {`${value.firstName} ${value.lastName}`}
            </MenuItem>
          );
        })}
      </Menu>
    </>
  );
};
