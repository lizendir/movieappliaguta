import React from 'react';
import { Container, Typography } from '@material-ui/core';

import { useStyles } from '../styles/contentStyles';

export default function Top() {
  const { top } = useStyles();

  return (
    <Container className={top} maxWidth="sm">
      <Typography
        component="h3"
        variant="h3"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Movies for You
      </Typography>
      <Typography variant="h5" align="center" color="textSecondary" paragraph>
        Watch films in our application
      </Typography>
    </Container>
  );
}
