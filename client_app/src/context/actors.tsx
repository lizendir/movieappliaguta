import React from 'react';
import { Actor } from '../models/MovieModels';

type ContextType = {
  actors: Actor[] | null;
  setActors: (actors: Actor[]) => void;
};

const initialState: ContextType = {
  actors: null,
  setActors: (actors: Actor[]) => {
    return;
  }
};

export const ActorsContext = React.createContext<ContextType>(initialState);

function actorsReducer(
  state: ContextType,
  action: { payload: any; type: string }
) {
  switch (action.type) {
    case 'SET_ACTORS':
      return {
        ...state,
        actors: action.payload
      };
    default:
      return state;
  }
}

export function ActorsProvider(props: any) {
  const [state, dispatch] = React.useReducer(actorsReducer, initialState);

  function setActors(actors: Actor[]) {
    dispatch({
      type: 'SET_ACTORS',
      payload: actors
    });
  }

  return (
    <ActorsContext.Provider
      value={{ actors: state.actors, setActors }}
      {...props}
    />
  );
}
