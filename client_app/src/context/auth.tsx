import React from 'react';
import jwtDecode from 'jwt-decode';

interface InitialStateProps {
  user: null | {};
}

const initialState: InitialStateProps = {
  user: null
};

const token = localStorage.getItem('jwtToken');

if (token) {
  const decodedToken: { exp: number } = jwtDecode(token);

  if (decodedToken.exp * 1000 < Date.now()) {
    localStorage.removeItem('jwtToken');
  } else {
    initialState.user = decodedToken;
  }
}

interface LoginData {
  token: string;
  username: string;
  email: string;
}

interface UserData {
  username: string;
}

type ContextType = {
  user: null | UserData;
  login: (userData: LoginData) => {};
  logout: () => {};
};

export const AuthContext = React.createContext<ContextType | undefined>(
  undefined
);

function authReducer(state: { user: string }, action: any) {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload
      };
    case 'LOGOUT':
      return {
        ...state,
        user: null
      };
    default:
      return state;
  }
}

export function AuthProvider(props: any) {
  const [state, dispatch] = React.useReducer(authReducer, initialState);

  function login(userData: LoginData) {
    localStorage.setItem('jwtToken', userData.token);
    dispatch({
      type: 'LOGIN',
      payload: userData
    });
  }

  function logout() {
    localStorage.removeItem('jwtToken');
    dispatch({ type: 'LOGOUT' });
  }

  return (
    <AuthContext.Provider
      value={{ user: state.user, login, logout }}
      {...props}
    />
  );
}
