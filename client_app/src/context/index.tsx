import * as React from 'react';
import { ActorsProvider } from './actors';
import { MovieProvider } from './movieCard';
import { AuthProvider } from './auth';

const ContextProvider: React.FC<{}> = ({ children }) => {
  return (
    <ActorsProvider>
      <AuthProvider>
        <MovieProvider>{children}</MovieProvider>
      </AuthProvider>
    </ActorsProvider>
  );
};

export default ContextProvider;
