import React from 'react';
import { Actor } from '../models/MovieModels';

export interface MovieInput {
  id?: number;
  title: string;
  description: string;
  year: number;
  photo?: string;
  genres: string[];
  cast: Actor[];
}

type actionType = 'Add' | 'Edit';

export const initialMovie: MovieInput = {
  title: '',
  description: '',
  year: 1895,
  photo: '',
  genres: [],
  cast: [],
};

type ContextType = {
  movie: MovieInput;
  actionType: actionType;
  setCard: (movie: MovieInput) => MovieInput;
  setActionType: (actionType: actionType) => actionType;
};

const initialState: ContextType = {
  movie: initialMovie,
  actionType: 'Add',
  setCard: (movie: MovieInput) => initialMovie,
  setActionType: (actionType: actionType) => actionType,
};

export const MovieContext = React.createContext<ContextType>(initialState);

function authReducer(
  state: ContextType,
  action: { payload: any; type: string },
) {
  switch (action.type) {
    case 'SET_MOVIE':
      return {
        ...state,
        movie: action.payload,
      };
    case 'SET_ACTION_TYPE':
      return {
        ...state,
        actionType: action.payload,
      };
    default:
      return state;
  }
}

export function MovieProvider(props: any) {
  const [state, dispatch] = React.useReducer(authReducer, initialState);

  function setCard(movie: MovieInput) {
    dispatch({
      type: 'SET_MOVIE',
      payload: movie,
    });
  }

  function setActionType(actionType: actionType) {
    dispatch({
      type: 'SET_ACTION_TYPE',
      payload: actionType,
    });
  }

  return (
    <MovieContext.Provider
      value={{
        movie: state.movie,
        actionType: state.actionType,
        setCard,
        setActionType,
      }}
      {...props}
    />
  );
}
