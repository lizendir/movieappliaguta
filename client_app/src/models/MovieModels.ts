export interface Actor {
  id: number;
  firstName: string;
  lastName: string;
  fullName: string;
  birthday?: Date;
  movies?: Movie[];
  createdAt: Date;
  updatedAt: Date;
}

export interface ActorInput {
  id: number;
  fullName: string;
  birthday: Date;
}

export interface Genre {
  id: number;
  name: Genres;
  movies?: Movie[];
  createdAt: Date;
  updatedAt: Date;
}

export interface Movie {
  id: number;
  title: string;
  description: string;
  rate: number;
  year: number;
  cast?: Actor[];
  genres?: Genre[];
  photo?: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface QueryMovieData {
  getMovies: Movie[];
  getMoviesByActor: Movie[];
  getMoviesByGenre: Movie[];
}

export interface QueryNavBarData {
  getGenres: Genre[];
  getActors: Actor[];
}

export enum Genres {
  'Action',
  'Adventure',
  'Comedy',
  'Crime',
  'Drama',
  'Fantasy',
  'Historical',
  'Horror',
  'Mystery',
  'Political',
  'Romance',
  'Saga',
  'Satire',
  'Social',
  'Speculative',
  'Thriller',
  'Urban',
  'Western'
}

export const GenresArray = [
  'Action',
  'Adventure',
  'Comedy',
  'Crime',
  'Drama',
  'Fantasy',
  'Historical',
  'Horror',
  'Mystery',
  'Political',
  'Romance',
  'Saga',
  'Satire',
  'Social',
  'Speculative',
  'Thriller',
  'Urban',
  'Western'
];
