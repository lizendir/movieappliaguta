import React, { useContext, useState } from 'react';
import {
  FormControl,
  InputLabel,
  Input,
  Button,
  CircularProgress,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { useMutation } from 'react-apollo';

import { AuthContext } from '../context/auth';
import { useStyles } from '../styles/contentStyles';
import { isString } from 'util';
import { LOGIN_USER_QUERY } from '../queries/LoginUserQuery';

export default function Login(props: any) {
  const context = useContext(AuthContext);
  const [loginValues, setLoginValues] = useState({
    username: '',
    password: '',
  });
  const [errors, setErrors] = useState({});
  const classes = useStyles();

  const [loginUser, { loading }] = useMutation(LOGIN_USER_QUERY, {
    update(_, { data: { login: userData } }) {
      setErrors({});
      context?.login(userData);
      props.history.push('/');
    },
    onError(err) {
      if (err.graphQLErrors[0].extensions?.exception.errors)
        setErrors(err.graphQLErrors[0].extensions?.exception.errors);
    },
    variables: loginValues,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLoginValues({
      ...loginValues,
      [event.target.name]: event.target.value,
    });
  };

  const onSubmit = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    loginUser();
  };

  if (loading) {
    return (
      <div className={classes.progress}>
        <CircularProgress />
      </div>
    );
  }

  return (
    <>
      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        aria-describedby="progress"
      >
        <FormControl error={Object.keys(errors).includes('username')}>
          <InputLabel>User Name</InputLabel>
          <Input
            name="username"
            value={loginValues.username}
            onChange={handleChange}
          />
        </FormControl>

        <FormControl error={Object.keys(errors).includes('password')}>
          <InputLabel> Password </InputLabel>
          <Input
            name="password"
            type="password"
            value={loginValues.password}
            onChange={handleChange}
          />
        </FormControl>
        <Button type="submit" onClick={onSubmit}>
          Login
        </Button>
        {Object.keys(errors).length > 0 && (
          <div>
            <ul>
              {Object.values(errors).map((error, index) => (
                <Alert key={index} severity="error">
                  {isString(error) ? error : 'An error occured'}
                </Alert>
              ))}
            </ul>
          </div>
        )}
      </form>
    </>
  );
}
