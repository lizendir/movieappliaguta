import React, { useContext } from 'react';
import {
  Card,
  CardMedia,
  Button,
  Typography,
  Grid,
  Container,
  FormGroup,
  CircularProgress,
} from '@material-ui/core';
import { useMutation } from 'react-apollo';

import { GenresArray } from '../models/MovieModels';
import { FormInput } from '../components/FormInput';
import { MultipleSelect } from '../components/MultipleSelect';
import { ErrorLabels } from '../components/ErrorLabels';
import { ActorsContext } from '../context/actors';
import { MovieContext, initialMovie } from '../context/movieCard';
import { useStyles } from '../styles/contentStyles';
import { EDIT_MOVIE_MUTATION } from '../queries/EditMovieMutation';
import { ADD_MOVIE_MUTATION } from '../queries/AddMovieMutation';
import { validateMovieForm } from '../utils/validateMovieForm';
import { GET_MOVIES } from '../queries/MovieQuery';

export default (props: any) => {
  const classes = useStyles();
  const movieContext = useContext(MovieContext);
  const actorsContext = useContext(ActorsContext);

  const movie = movieContext?.movie;
  const actors = actorsContext.actors || [];

  const actorsFullNames = actors.map((actor) => {
    return actor.fullName;
  });
  const actorsInMovie = movie?.cast
    ? movie.cast.map((actor) => {
        return actor.fullName;
      })
    : [];

  const castInput = movie?.cast.map((actor) => {
    return {
      id: actor.id,
      firstName: actor.firstName,
      lastName: actor.lastName,
      birthday: actor.birthday,
    };
  });

  const MUTATION =
    movieContext.actionType === 'Add'
      ? ADD_MOVIE_MUTATION
      : EDIT_MOVIE_MUTATION;

  const [mutateMovie, { loading }] = useMutation(MUTATION, {
    update(proxy, result) {
      const data: any = proxy.readQuery({
        query: GET_MOVIES,
        variables: null,
      });
      data.getMovies = [result.data.addMovie, ...data.getMovies];
      proxy.writeQuery({ query: GET_MOVIES, data });
      props.history.push('/');
      movieContext.setCard(initialMovie);
    },
    onError(err) {
      console.log(err);
    },
    variables: {
      id: movie?.id,
      movie: {
        title: movie?.title,
        description: movie?.description,
        year: movie?.year,
        photo: movie?.photo,
        genres: movie?.genres,
        cast: castInput,
      },
    },
  });

  const validateNumberInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.match('^[0-9]*$') && movie) {
      movieContext.setCard({
        ...movie,
        [event.target.name]: +event.target.value,
      });
    }
  };

  const onChangeGenres = (event: React.ChangeEvent<{ value: unknown }>) => {
    if (movie)
      movieContext?.setCard({
        ...movie,
        genres: event.target.value as string[],
      });
  };

  const onChangeActors = (event: React.ChangeEvent<{ value: unknown }>) => {
    if (movie) {
      const values = event.target.value as string[];
      const cast = actors.filter((actor) => {
        const match = values.map((value) => {
          if (actor.fullName === value) return true;
          else return null;
        });
        if (match.includes(true)) return actor;
        return null;
      });
      movieContext?.setCard({
        ...movie,
        cast,
      });
    }
  };

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (movie)
      movieContext?.setCard({
        ...movie,
        [event.target.name]: event.target.value,
      });
  };

  const [errors, setErrors] = React.useState({
    description: null,
    title: null,
    year: null,
  });

  const onSubmit = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    const validateErrors = validateMovieForm(movie);
    if (validateErrors) setErrors(validateErrors);
    else mutateMovie();
  };

  if (loading)
    return (
      <div className={classes.progress}>
        <CircularProgress />
      </div>
    );

  return movie ? (
    <form className={classes.form} noValidate autoComplete="off">
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            <CardMedia
              className={classes.cardFormMedia}
              image={
                movie.photo ||
                'https://polycar.com.ua/wp-content/uploads/2019/07/no-photo-polycar-300x210.png'
              }
              title={movie.title}
            />
          </Card>
        </Grid>
      </Container>
      <FormGroup style={{ width: 400 }}>
        <FormInput
          label="Title"
          name="title"
          value={movie.title}
          isError={errors.title ? true : false}
          onChange={onChange}
        />
        <FormInput
          label="Description"
          name="description"
          value={movie.description}
          isError={errors.description ? true : false}
          onChange={onChange}
          multiline={true}
        />
        <FormInput
          label="Year"
          name="year"
          value={movie.year.toString()}
          isError={errors.year ? true : false}
          onChange={validateNumberInput}
        />
        <FormInput
          label="Photo URL"
          name="photo"
          value={movie.photo || ''}
          onChange={onChange}
        />
        <MultipleSelect
          label="Genres"
          arrayOfChosen={movie.genres}
          arrayValues={GenresArray}
          onChange={onChangeGenres}
        />
        <MultipleSelect
          label="Cast"
          arrayOfChosen={actorsInMovie}
          arrayValues={actorsFullNames}
          onChange={onChangeActors}
        />
      </FormGroup>
      <Button type="submit" onClick={onSubmit} variant="outlined">
        {movieContext.actionType}
      </Button>
      <ErrorLabels errors={errors} />
    </form>
  ) : (
    <Typography gutterBottom variant="h5" component="h2" align="center">
      No movie selected
    </Typography>
  );
};
