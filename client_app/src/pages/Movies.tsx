import React, { useContext } from 'react';
import {
  Container,
  Grid,
  CardMedia,
  CardContent,
  Typography,
  Button,
  CardActions,
  Card,
  Divider,
  Chip,
} from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useQuery } from 'react-apollo';
import { DocumentNode } from 'graphql';

import { QueryMovieData, Movie } from '../models/MovieModels';
import { useStyles } from '../styles/contentStyles';
import { MovieContext, MovieInput, initialMovie } from '../context/movieCard';
import { AuthContext } from '../context/auth';
import AddButton from '../components/AddButton';

const message = (text: string) => {
  return (
    <Typography variant="h5" component="h2" align="center">
      {text}
    </Typography>
  );
};

interface CardsAttr {
  props: any;
  QUERY: DocumentNode;
  variables: {} | undefined;
}

export default function Movies({ props, QUERY, variables }: CardsAttr) {
  const classes = useStyles();

  const movieContext = useContext(MovieContext);
  const authContext = useContext(AuthContext);

  const { data, error, loading } = useQuery<QueryMovieData>(QUERY, {
    variables,
  });

  const setMovies = (data: QueryMovieData) => {
    if (data.getMovies) {
      return data.getMovies;
    } else if (data.getMoviesByActor) {
      return data.getMoviesByActor;
    } else if (data.getMoviesByGenre) {
      return data.getMoviesByGenre;
    }
    return [];
  };

  const onClickEdit = (movie: Movie) => {
    if (movie.genres) {
      const genres = movie.genres.map((genre) => {
        return genre.name.toString();
      });
      movieContext.setCard({ ...movie, genres } as MovieInput);
      movieContext.setActionType('Edit');
      props.history.push('/card');
    }
  };

  const onClickAdd = () => {
    movieContext.setCard(initialMovie);
    movieContext.setActionType('Add');
    props.history.push('/card');
  };

  if (loading) {
    return (
      <div className={classes.progress}>
        <CircularProgress />
      </div>
    );
  }
  if (error || !data) {
    if (error?.message) {
      return message(error?.message);
    } else {
      return message('No data');
    }
  }

  const movies = setMovies(data);

  if (movies.length < 1) return message('No Movies');

  return (
    <>
      {authContext?.user ? <AddButton onClickAdd={onClickAdd} /> : null}
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid className={classes.cardGrid} container spacing={4}>
          {movies.map((movie, index) => {
            return (
              <Grid item key={index} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={
                      movie.photo ||
                      'https://polycar.com.ua/wp-content/uploads/2019/07/no-photo-polycar-300x210.png'
                    }
                    title={movie.title}
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {movie.title}
                    </Typography>
                    <Typography>{movie.description}</Typography>
                    <Divider className={classes.divider} />
                    Genre:
                    {movie.genres?.map((genre, index) => {
                      return (
                        <Chip
                          className={classes.chip}
                          label={genre.name}
                          key={index}
                        />
                      );
                    })}
                    <Divider className={classes.divider} />
                    Cast:
                    {movie.cast?.map((actor, index) => {
                      return (
                        <Chip
                          className={classes.chip}
                          label={actor.fullName}
                          key={index}
                        />
                      );
                    })}
                  </CardContent>
                  <CardActions>
                    <Button size="small" color="primary" disabled>
                      Watch
                    </Button>
                    <Button size="small" color="primary" disabled>
                      {movie.year}
                    </Button>
                    {authContext?.user && (
                      <Button
                        size="small"
                        color="primary"
                        onClick={(e) => onClickEdit(movie)}
                      >
                        Edit
                      </Button>
                    )}
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
        </Grid>
      </Container>
    </>
  );
}
