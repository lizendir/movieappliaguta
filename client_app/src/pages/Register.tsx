import React, { useContext, useState } from 'react';
import {
  FormControl,
  InputLabel,
  Input,
  Button,
  CircularProgress,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { useMutation } from 'react-apollo';
import { isString } from 'util';

import { AuthContext } from '../context/auth';
import { useStyles } from '../styles/contentStyles';
import { REGISTER_USER_QUERY } from '../queries/RegisterQuery';

export default function Register(props: any) {
  const context = useContext(AuthContext);

  const [registerValues, setRegisterValues] = useState({
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
  });
  const [errors, setErrors] = useState({});
  const classes = useStyles();

  const [addUser, { loading }] = useMutation(REGISTER_USER_QUERY, {
    update(_, { data: { register: userData } }) {
      setErrors({});
      context?.login(userData);
      props.history.push('/');
    },
    onError(err) {
      if (err.graphQLErrors[0].extensions?.exception.errors)
        setErrors(err.graphQLErrors[0].extensions?.exception.errors);
    },
    variables: registerValues,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRegisterValues({
      ...registerValues,
      [event.target.name]: event.target.value,
    });
  };

  const onSubmit = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    addUser();
  };

  if (loading) {
    return (
      <div className={classes.progress}>
        <CircularProgress />
      </div>
    );
  }

  return (
    <>
      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        aria-describedby="progress"
      >
        <FormControl error={Object.keys(errors).includes('username')}>
          <InputLabel>User Name</InputLabel>
          <Input
            name="username"
            value={registerValues.username}
            onChange={handleChange}
          />
        </FormControl>

        <FormControl error={Object.keys(errors).includes('email')}>
          <InputLabel> Email </InputLabel>
          <Input
            name="email"
            value={registerValues.email}
            onChange={handleChange}
          />
        </FormControl>

        <FormControl error={Object.keys(errors).includes('password')}>
          <InputLabel> Password </InputLabel>
          <Input
            name="password"
            type="password"
            value={registerValues.password}
            onChange={handleChange}
          />
        </FormControl>

        <FormControl error={Object.keys(errors).includes('confirmPassword')}>
          <InputLabel>Confirm Password</InputLabel>
          <Input
            name="confirmPassword"
            type="password"
            value={registerValues.confirmPassword}
            onChange={handleChange}
          />
        </FormControl>
        <Button type="submit" onClick={onSubmit}>
          Submit
        </Button>
        {Object.keys(errors).length > 0 && (
          <div>
            <ul>
              {Object.values(errors).map((error, index) => (
                <Alert key={index} severity="error">
                  {isString(error) ? error : 'An error occured'}
                </Alert>
              ))}
            </ul>
          </div>
        )}
      </form>
    </>
  );
}
