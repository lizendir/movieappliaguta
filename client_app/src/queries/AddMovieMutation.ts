import gql from 'graphql-tag';
import { MovieFieldsFragment } from './fragments';

export const ADD_MOVIE_MUTATION = gql`
  mutation addMovie($movie: MovieInput!) {
    addMovie(movie: $movie) {
      ...MovieFieldsFragment
    }
  }
  ${MovieFieldsFragment}
`;
