import gql from 'graphql-tag';
import { MovieFieldsFragment } from './fragments';

export const EDIT_MOVIE_MUTATION = gql`
  mutation editMovie($movie: MovieInput!, $id: Int!) {
    updateMovie(movie: $movie, id: $id) {
      ...MovieFieldsFragment
    }
  }
  ${MovieFieldsFragment}
`;
