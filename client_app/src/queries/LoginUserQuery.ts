import gql from 'graphql-tag';

export const LOGIN_USER_QUERY = gql`
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      email
      createdAt
      username
      token
    }
  }
`;
