import gql from 'graphql-tag';

import { MovieFieldsFragment } from './fragments';

export const GET_MOVIES = gql`
  query getMovies {
    getMovies {
      ...MovieFieldsFragment
    }
  }
  ${MovieFieldsFragment}
`;
