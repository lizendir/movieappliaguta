import gql from 'graphql-tag';

import { MovieFieldsFragment } from './fragments';

export const NAVBAR_QUERY = gql`
  query {
    getGenres {
      name
      createdAt
      updatedAt
    }
    getActors {
      id
      firstName
      lastName
      fullName
      birthday
    }
  }
`;

export const GET_MOVIES_BY_ACTOR_QUERY = gql`
  query($id: Int!) {
    getMoviesByActor(actorId: $id) {
      ...MovieFieldsFragment
    }
  }
  ${MovieFieldsFragment}
`;

export const GET_MOVIES_BY_GENRE_QUERY = gql`
  query($genre: String!) {
    getMoviesByGenre(genre: $genre) {
      ...MovieFieldsFragment
    }
  }
  ${MovieFieldsFragment}
`;
