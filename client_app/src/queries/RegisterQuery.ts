import gql from 'graphql-tag';

export const REGISTER_USER_QUERY = gql`
  mutation register(
    $username: String!
    $email: String!
    $password: String!
    $confirmPassword: String!
  ) {
    register(
      registerInput: {
        username: $username
        email: $email
        password: $password
        confirmPassword: $confirmPassword
      }
    ) {
      email
      createdAt
      username
      token
    }
  }
`;
