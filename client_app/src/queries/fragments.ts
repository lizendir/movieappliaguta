import gql from 'graphql-tag';

export const MovieFieldsFragment = gql`
  fragment MovieFieldsFragment on Movie {
    id
    title
    description
    rate
    year
    photo
    genres {
      name
    }
    cast {
      id
      lastName
      firstName
      fullName
      birthday
    }
  }
`;
