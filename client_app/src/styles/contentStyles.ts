import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  /* Root  */

  container: {
    width: '100%',
    maxWidth: 1000,
    margin: 'auto',
  },

  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },

  /* Top */

  top: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 40,
    marginTop: 20,
  },

  /* Nav */

  navbarTitle: {
    marginRight: 10,
    color: 'white',
    cursor: 'default',
  },

  icon: {
    marginRight: 10,
    backgroundColor: '#3f51b5',
  },

  logRegTabs: {
    margin: theme.spacing(1),
    marginLeft: 'auto',
  },

  /* Button */

  btn: {
    color: '#fff',
    minWidth: 86,
    marginRight: 10,
  },

  /*Card */

  cardGrid: {
    display: 'flex',
    justifyContent: 'center',
    paddingBottom: theme.spacing(8),
  },

  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 345,
    margin: 'auto',
  },

  cardMedia: {
    paddingTop: '150%',
  },

  cardFormMedia: {
    paddingBottom: theme.spacing(4),
    paddingTop: '150%',
  },

  cardContent: {
    flexGrow: 1,
  },

  /* Chip */

  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  chip: {
    margin: 2,
  },

  divider: {
    margin: 5,
  },
  progress: {
    display: 'flex',
    justifyContent: 'center',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    '& > *': {
      margin: theme.spacing(1),
    },
    alignItems: 'center',
  },
}));
