import { MovieInput } from '../context/movieCard';

export function validateMovieForm(movie: MovieInput) {
  let validateErrors: any;
  if (movie?.description === '')
    validateErrors = { description: 'Description must be not empty' };
  if (movie?.title === '')
    validateErrors = { ...validateErrors, title: 'Title must be not empty' };
  if (movie?.year.toString() === '')
    validateErrors = { ...validateErrors, year: 'Year must be not empty' };
  return validateErrors;
}
