import { SchemaDirectiveVisitor } from 'apollo-server-express';
import { defaultFieldResolver } from 'graphql';
import checkAuth from '../util/checkAuth';

class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: any) {
    const originalResolve = field.resolve || defaultFieldResolver;

    field.resolve = function (...args: any[]) {
      const context = args[2];
      checkAuth(context);

      return originalResolve.apply(this, args);
    };
  }
}

export default AuthDirective;
