import { gql } from 'apollo-server';

export default gql`
  type User {
    email: String!
    token: String!
    username: String!
    createdAt: Date!
  }
  input RegisterInput {
    username: String!
    password: String!
    confirmPassword: String!
    email: String!
  }
  extend type Mutation {
    register(registerInput: RegisterInput): User!
    login(username: String!, password: String!): User!
  }
`;
