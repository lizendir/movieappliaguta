import { gql } from 'apollo-server';

export default gql`
  scalar Date

  directive @auth on FIELD_DEFINITION

  enum Genres {
    Action
    Adventure
    Comedy
    Crime
    Drama
    Fantasy
    Historical
    Horror
    Mystery
    Political
    Romance
    Saga
    Satire
    Social
    Speculative
    Thriller
    Urban
    Western
  }

  type Genre {
    name: Genres!
    movies: [Movie!]
    createdAt: Date!
    updatedAt: Date!
  }

  type Movie {
    id: Int!
    title: String!
    description: String
    year: Int!
    rate: Float
    genres: [Genre!]
    cast: [Actor!]
    photo: String
    createdAt: Date!
    updatedAt: Date!
  }

  input MovieInput {
    title: String!
    description: String
    year: Int!
    photo: String
    genres: [String!]
    cast: [ActorInput!]
  }

  type Actor {
    id: Int!
    firstName: String!
    lastName: String!
    fullName: String!
    birthday: Date!
    movies: [Movie!]
    createdAt: Date!
    updatedAt: Date!
  }

  input ActorCreateInput {
    firstName: String!
    lastName: String!
    birthday: Date!
  }

  input ActorInput {
    id: Int!
    firstName: String
    lastName: String
    birthday: Date
  }

  type Query {
    getMovies: [Movie!]
    getMoviesByGenre(genre: String!): [Movie!]
    getMoviesByActor(actorId: Int!): [Movie!]
    getMovie(id: Int!): Movie

    getActors: [Actor!]
    getActor(id: Int!): Actor

    getGenres: [Genre!]!
  }

  type Mutation {
    addMovie(movie: MovieInput!): Movie @auth
    updateMovie(movie: MovieInput!, id: Int!): Movie @auth
    deleteMovie(id: Int!): String @auth

    addActor(actor: ActorCreateInput!): Actor @auth
    updateActor(actor: ActorInput!): Actor @auth
    deleteActor(id: Int!): String @auth
  }
`;
