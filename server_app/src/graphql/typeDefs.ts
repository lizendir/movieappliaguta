import authSchema from './AuthenticationSchema';
import movieSchema from './MovieAppSchema';

export default [authSchema, movieSchema];
