import { ApolloServer } from 'apollo-server';

import resolvers from './resolvers';
import typeDefs from './graphql/typeDefs';
import { sequelize } from './sequelize';
import { initDB } from './inits/initDB';
import AuthDirective from './directives/AuthDirective';

const models = sequelize.models;

sequelize.sync({ force: true }).then(() => {
  initDB();
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  schemaDirectives: {
    auth: AuthDirective
  },
  context: ({ req }) => ({ models, req })
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
