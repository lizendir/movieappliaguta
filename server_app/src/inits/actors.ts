export const actors = [
  {
    firstName: 'Leonardo',
    lastName: 'DiCaprio',
    birthday: '1974-11-11'
  },
  {
    firstName: 'Kate',
    lastName: 'Winslet',
    birthday: '1975-10-5'
  },
  {
    firstName: 'Billy',
    lastName: 'Zane',
    birthday: '1966-02-24'
  },
  {
    firstName: 'Brad',
    lastName: 'Pitt',
    birthday: '1963-12-18'
  },
  {
    firstName: 'Margot',
    lastName: 'Robbie',
    birthday: '1990-07-02'
  },
  {
    firstName: 'Matthew',
    lastName: 'McConaughey',
    birthday: '1969-11-04'
  },
  {
    firstName: 'Jonah',
    lastName: 'Hill',
    birthday: '1983-12-20'
  }
];
