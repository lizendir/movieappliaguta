import { Actor } from '../models/Actor';
import { Genre } from '../models/Genre';
import { Movie } from '../models/Movie';
import { actors } from './actors';
import { genres } from './genres';

export const initDB = async () => {
  const titanic = await Movie.create({
    photo: 'https://www.kinopoisk.ru/images/film_big/2213.jpg',
    title: 'Titanic',
    description:
      'Titanic is a 1997 American epic romance and disaster film directed, written, co-produced, and co-edited by James Cameron.',
    rate: 8,
    year: 1997
  });
  const inHollywood = await Movie.create({
    photo:
      'https://thumbnails.moviemania.io/phone/movie/466272/bf6b52/670x1192.jpg',
    title: 'Once Upon a Time in Hollywood',
    description:
      'Once Upon a Time in Hollywood[a] is a 2019 comedy-drama film written and directed by Quentin Tarantino. Produced by Columbia Pictures, Bona Film Group, Heyday Films, and Visiona Romantica and distributed by Sony Pictures Releasing',
    rate: 7,
    year: 2019
  });
  const wallStreet = await Movie.create({
    photo:
      'https://target.scene7.com/is/image/Target/GUEST_e612fdfb-9fab-41e4-a006-4993dd9d2663?fmt=webp&wid=1400&qlt=80',
    title: 'The Wolf of Wall Street',
    description:
      'The Wolf of Wall Street is a 2013 American biographical black comedy crime film directed by Martin Scorsese and written by Terence Winter, based on the 2007 memoir of the same name by Jordan Belfort.',
    rate: 9,
    year: 2013
  });

  await Actor.bulkCreate(actors);

  await Genre.bulkCreate(genres);

  await Actor.findAll({
    where: {
      lastName: ['DiCaprio', 'Winslet', 'Zane']
    }
  }).then((res) => {
    res?.map((value, index, array) => {
      value.$add('Movies', [titanic]);
    });
  });

  await Actor.findAll({
    where: {
      lastName: ['DiCaprio', 'Pitt', 'Robbie']
    }
  }).then((res) => {
    res?.map((value, index, array) => {
      value.$add('Movies', [inHollywood]);
    });
  });

  await Actor.findAll({
    where: {
      lastName: ['DiCaprio', 'McConaughey', 'Robbie', 'Hill']
    }
  }).then((res) => {
    res?.map((value, index, array) => {
      value.$add('Movies', [wallStreet]);
    });
  });

  await titanic.$add('Genre', 'Drama');
  await inHollywood.$add('Genre', ['Drama', 'Comedy']);
  await wallStreet.$add('Genre', 'Crime');
};
