import {
  Model,
  Column,
  Table,
  BelongsToMany,
  CreatedAt,
  UpdatedAt
} from 'sequelize-typescript';

import { Movie } from './Movie';
import { MovieActor } from './MovieActor';

@Table
export class Actor extends Model<Actor> {
  @Column
  get fullName(): string {
    return this.getDataValue('firstName') + ' ' + this.getDataValue('lastName');
  }

  @Column
  firstName!: string;

  @Column
  lastName!: string;

  @Column
  birthday?: Date;

  @BelongsToMany(() => Movie, () => MovieActor)
  movies?: Movie[];

  @CreatedAt
  @Column
  createdAt!: Date;

  @UpdatedAt
  @Column
  updatedAt!: Date;
}
