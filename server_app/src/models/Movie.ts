import {
  BelongsToMany,
  Column,
  CreatedAt,
  Model,
  Table,
  UpdatedAt,
  Length,
  DataType
} from 'sequelize-typescript';
import { Actor } from './Actor';
import { Genre } from './Genre';
import { MovieActor } from './MovieActor';
import { MovieGenre } from './MovieGenre';

@Table
export class Movie extends Model<Movie> {
  @Column
  title!: string;

  @Column
  description?: string;

  @Length({ min: 0, max: 10 })
  @Column
  rate!: number;

  @BelongsToMany(() => Actor, () => MovieActor)
  cast?: Actor[];

  @BelongsToMany(() => Genre, () => MovieGenre)
  genres?: Genre[];

  @Column
  year!: number;

  @Column(DataType.TEXT)
  photo?: string;

  @CreatedAt
  createdAt!: Date;

  @UpdatedAt
  updatedAt!: Date;
}
