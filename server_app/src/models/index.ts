import { Request } from 'express';

import { Actor } from './Actor';
import { Genre } from './Genre';
import { Movie } from './Movie';
import { MovieActor } from './MovieActor';
import { MovieGenre } from './MovieGenre';
import { User } from './User';

export default [Actor, Genre, Movie, MovieActor, MovieGenre, User];

export interface ResolverModels {
  models: {
    Actor: typeof Actor;
    Genre: typeof Genre;
    Movie: typeof Movie;
  };
  req: Request;
}
