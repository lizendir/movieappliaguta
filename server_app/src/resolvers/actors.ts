import { IResolvers } from 'apollo-server';

import { ResolverModels } from '../models';
import { Actor } from '../models/Actor';
import { Movie } from '../models/Movie';
import { Genre } from '../models/Genre';

export const movieInclude = {
  model: Movie,
  include: [Genre, Actor]
};

export const actors: IResolvers<{}, ResolverModels> = {
  Query: {
    getActors(_, {}, ctx) {
      return ctx.models.Actor.findAll({
        include: [movieInclude]
      });
    },

    getActor(_, args: { id: number }, ctx) {
      return ctx.models.Actor.findByPk(args.id, {
        include: [movieInclude]
      });
    }
  },

  Mutation: {
    addActor(_, args: { actor: Actor }, ctx) {
      return ctx.models.Actor.create(args.actor);
    },

    async updateActor(_, args: { actor: Actor }, ctx) {
      const actorFind = await ctx.models.Actor.findByPk(args.actor.id);
      await actorFind?.update(args.actor);
      return actorFind;
    },

    deleteActor(_, args: { id: number }, ctx) {
      return ctx.models.Actor.destroy({ where: { id: args.id } }).catch(
        (err) => {
          return err;
        }
      );
    }
  }
};
