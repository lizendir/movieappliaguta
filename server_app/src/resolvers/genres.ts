import { IResolvers } from 'apollo-server';

import { ResolverModels } from '../models';

export const genre: IResolvers<{}, ResolverModels> = {
  Query: {
    getGenres(_, {}, ctx) {
      return ctx.models.Genre.findAll();
    }
  }
};
