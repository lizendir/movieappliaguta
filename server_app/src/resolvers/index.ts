import { actors } from './actors';
import { genre } from './genres';
import { movies } from './movies';
import { scalarTypes } from './scalarTypes';
import { users } from './users';

export default [actors, genre, movies, scalarTypes, users];
