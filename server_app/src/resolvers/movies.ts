import { IResolvers } from 'apollo-server';

import { ResolverModels } from '../models';
import { Actor } from '../models/Actor';
import { Genre } from '../models/Genre';
import { Movie } from '../models/Movie';
import { movieInclude } from './actors';

async function setRelatedTables(dataSource: Movie, movie: Movie) {
  if (dataSource.genres && dataSource.genres.length != 0) {
    await movie.$set('genres', dataSource.genres || '');
  }
  if (dataSource.cast) {
    const castIds = dataSource.cast.map((actor) => actor.id);
    await movie.$set('cast', castIds || '');
  }
}

export const movies: IResolvers<{}, ResolverModels> = {
  Query: {
    getMovies(_, {}, ctx) {
      return ctx.models.Movie.findAll({ include: [{ all: true }] });
    },

    getMoviesByGenre(_, args: { genre: string }, ctx) {
      return ctx.models.Genre.findOne({
        where: { name: args.genre },
        include: [movieInclude]
      }).then((genre) => {
        return genre?.movies;
      });
    },

    getMoviesByActor(_, args: { actorId: number }, ctx) {
      return ctx.models.Actor.findOne({
        where: { id: args.actorId },
        include: [movieInclude]
      }).then((actor) => {
        return actor?.movies;
      });
    },

    getMovie(_, args: { id: number }, ctx) {
      return ctx.models.Movie.findByPk(args.id, {
        include: [{ all: true }]
      });
    }
  },

  Mutation: {
    async addMovie(_, args: { movie: Movie }, ctx) {
      const movie = args.movie;

      const movieCreate = await ctx.models.Movie.create(movie);
      await setRelatedTables(movie, movieCreate);

      return await ctx.models.Movie.findByPk(movieCreate.id, {
        include: [Genre, Actor]
      });
    },

    async updateMovie(_, args: { movie: Movie; id: number }, ctx) {
      const movie = args.movie;
      const id = args.id;

      const movieFind = await ctx.models.Movie.findByPk(id, {
        include: [Genre, Actor]
      });
      if (!movieFind) throw Error('Movie not found');

      await movieFind?.update(movie);
      await setRelatedTables(movie, movieFind);

      return await ctx.models.Movie.findByPk(id, {
        include: [Genre, Actor]
      });
    },

    deleteMovie(_, args: { id: number }, ctx) {
      return ctx.models.Movie.destroy({ where: { id: args.id } }).catch(
        (err) => {
          return err;
        }
      );
    }
  }
};
