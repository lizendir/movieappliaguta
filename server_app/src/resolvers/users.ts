import jwt from 'jsonwebtoken';
import { UserInputError, IResolvers } from 'apollo-server';

import {
  validateLoginInput,
  validateRegisterInput,
  validateCredentials
} from '../util/validators';
import { User } from '../models/User';
import { ResolverModels } from '../models';
import { createUser } from '../util/createUser';

export interface TokenInterface {
  id: number;
  email: string;
  username: string;
}

function generateToken(user: User) {
  return jwt.sign(
    {
      id: user.id,
      email: user.email,
      username: user.username
    },
    process.env['REACT_APP_SECRET_KEY'] || 'some secret key',
    { expiresIn: '1h' }
  );
}

export const users: IResolvers<{}, ResolverModels> = {
  Mutation: {
    async login(_, { username, password }) {
      const user = await User.findOne({ where: { username } });
      if (user) {
        validateLoginInput(username, password);
        validateCredentials(user, password);

        const token = generateToken(user);
        return {
          ...user.toJSON(),
          token
        };
      } else {
        const errors = {
          general: 'User not found'
        };
        throw new UserInputError('User not found', { errors });
      }
    },

    async register(
      _,
      { registerInput: { username, email, password, confirmPassword } }
    ) {
      const user = await User.findOne({ where: { username } });
      if (user) {
        throw new UserInputError('Username is taken', {
          errors: {
            username: 'This username is taken'
          }
        });
      }

      validateRegisterInput(username, email, password, confirmPassword);

      // hash password and create an auth token
      const newUser = await createUser(password, email, username);

      const token = generateToken(newUser);
      return {
        ...newUser.toJSON(),
        token
      };
    }
  }
};
